i4oled-gui
==========

This project is a showcase of graphical user interface for setting OLEDs on Wacom Intuos4 tablets. It requires gnome & dconf, but it doesn't require root access rights. I hope that some of the ideas tested here will be used in gnome. 

Video demo: http://firszt.eu/wacom-icons/wacom_oleds_in_action.webm

![Demo](https://github.com/PrzemoF/i4oled-gui/blob/master/screenshot/demo.png)

For Fedora 20/21 users:

1. Install required packages:
``` console
sudo yum install git autoconf automake gcc gtk3-devel dconf-devel
```
2. Clone the repository
``` console
git clone https://github.com/PrzemoF/i4oled-gui.git
```
3. Enter & build
``` console
cd i4oled-gui
./autogen.sh
./configure
make
sudo make install
```
Message about missing gtk+-3.0 means that there is no gtk3-devel package.
Message about missing dconf means that there is no dconf-devel package.

Ubuntu uses gtk3-dev and dconf-dev packages.

Useful info:

1. User icons shall be placed in ~/.icons/wacom and have to be PNG image data, 64 x 32, 8-bit/color RGBA, non-interlaced

2. Script pixmaps/svg/svg2png.sh can convert SVG to desired format

