#!/bin/bash
ICON_FILE=i4oled-gui
ICON_PATH=scalable/
SIZE_LIST=$(ls -d *x*)
for size in $SIZE_LIST; do
	convert -density 1000x1000 -background none -gravity center $ICON_PATH$ICON_FILE'.svg' -geometry $size -extent $size png32:$size/apps/$ICON_FILE.png;
done;
