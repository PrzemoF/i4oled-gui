#!/bin/bash
for i in $(ls *.svg); do convert -density 1000x1000 -background black -modulate 200 -gravity center $i -geometry 64x31  -extent 64x32 png32:$i.png; done;
rename .svg.png .png *
mv *.png ../
