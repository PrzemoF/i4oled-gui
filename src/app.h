#ifndef __APP__
#define __APP__

#include <gtk/gtk.h>
#include "dconf.h"

#define UI_DEFINITIONS_FILE UI_DIR "/ui.glade"
#define WACOM_DEV_DIR "/org/gnome/settings-daemon/peripherals/wacom/"
#define WACOM_ICONS_USER_DIR ".icons/wacom/"
//FIXME Is it really the best solution?
#define WACOM_ICONS_SYS_DIR "/usr/local/share/i4oled-gui/pixmaps/"
#define WACOM_ICONS_SYS_LOCAL_DIR "/usr/share/i4oled-gui/pixmaps/"
#define WACOM_OLED_KEY "oled-label"

#define OLED_WIDTH 64
#define OLED_HEIGHT 32
#define ICON_PADDING 10
#define MAGIC_BASE64 "base64:"
#define MAGIC_BASE64_LEN strlen(MAGIC_BASE64)
#define MAX_IMAGE_SIZE 1024
#define USB_PIXELS_PER_BYTE 2
#define BT_PIXELS_PER_BYTE 8
#define USB_BUF_LEN OLED_HEIGHT * OLED_WIDTH / USB_PIXELS_PER_BYTE
#define LABEL_SIZE              30                      /*Maximum length of text for OLED icon*/
#define MAX_1ST_LINE_LEN        10                      /*Maximum number of characters in 1st line of OLED icon*/
#define MAX_TOKEN               (LABEL_SIZE >> 1)       /*Maximum number of tokens equals half of maximum number of characters*/

#define GET_UI_ELEMENT(TYPE, ELEMENT)   TYPE *ELEMENT = (TYPE *) app_get_ui_element(app, #ELEMENT);

enum
{
	COL_PIXBUF,
	NUM_COLS
};

typedef struct app_
{
	GtkBuilder 	*definitions;
	GSList 		*objects;
	
	int 		critical_error;
	gchar 		*dconf_dir;
	DConfClient 	*dconf_client;
	unsigned char 	*image;
	gchar 		*current_button;

} t_app;

GObject *app_get_ui_element (t_app*, const gchar*);
void app_init (t_app*);
void create_window(t_app*);
GtkTreeModel *create_and_fill_model (t_app*);
GtkTreeModel *i4_create_model (void);
void gui_init_from_dconf(t_app*);

void i4_dconf_write (gchar*, const gchar*, t_app*);
void i4_set_text_for_button (gchar*, gchar*, t_app*);
void i4_set_pixbuf_for_button (gchar*, GdkPixbuf*, t_app*);
void i4_set_pixbuf_from_label (gchar*, gchar*, t_app*);

#endif
