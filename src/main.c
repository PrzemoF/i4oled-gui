#include "app.h"

int
main (int argc, char *argv[])
{
	t_app *app;

	app = (t_app *) g_new (t_app, 1);
	app->critical_error = 0;
	gtk_init (&argc, &argv);
	app_init (app);
	GET_UI_ELEMENT (GtkWidget, app_window);
	gui_init_from_dconf(app);
	if (!app->critical_error) {
		gtk_widget_show_all (app_window);
		gtk_main ();
	}

	return 0;
}
