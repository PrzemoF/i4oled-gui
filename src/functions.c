#include <math.h>
#include <string.h>
#include <stdlib.h>

#include "app.h"

gchar *
i4_strip_last(gchar *string)
{
	gchar *string_short;
	int len;
	if (string) {
		len =  strlen(string) - 1;
		string_short = g_strndup (string, len);
		return string_short;
	}
	return NULL;
}

void
i4_scramble_image (guchar* image)
{
	guchar buf[USB_BUF_LEN];
	int x, y, i;
	guchar l1, l2, h1, h2;

	for (i = 0; i < USB_BUF_LEN; i++)
		buf[i] = image[i];

	for (y = 0; y < (OLED_HEIGHT / 2); y++) {
		for (x = 0; x < (OLED_WIDTH / 2); x++) {
			l1 = (0x0F & (buf[OLED_HEIGHT - 1 - x + OLED_WIDTH * y]));
			l2 = (0x0F & (buf[OLED_HEIGHT - 1 - x + OLED_WIDTH * y] >> 4));
			h1 = (0xF0 & (buf[OLED_WIDTH - 1 - x + OLED_WIDTH * y] << 4));
			h2 = (0xF0 & (buf[OLED_WIDTH - 1 - x + OLED_WIDTH * y]));

			image[2 * x + OLED_WIDTH * y] = h1 | l1;
			image[2 * x + 1 + OLED_WIDTH * y] = h2 | l2;
		}
	}
}

void
i4_descramble_image (guchar* image)
{
	guchar buf[USB_BUF_LEN];
	int x, y, i;
	guchar l1, l2, h1, h2;

	for (i = 0; i < USB_BUF_LEN; i++)
		buf[i] = image[i];

	for (y = 0; y < (OLED_HEIGHT / 2); y++) {
		for (x = 0; x < (OLED_WIDTH / 2); x++) {
			l1 = 0x0F & (buf[2 * x + OLED_WIDTH * y]);
			l2 = (0xF0 & buf[2 * x + OLED_WIDTH * y]) >> 4;
			h1 = (0x0F & buf[2 * x + 1 + OLED_WIDTH * y]) << 4;
			h2 = 0xF0 & (buf[2 * x + 1 + OLED_WIDTH * y]);
			image[OLED_WIDTH - 1 - x + OLED_WIDTH * y] = h2 | l2;
			image[OLED_HEIGHT - 1 - x + OLED_WIDTH * y] =  h1 | l1;
		}
	}
}

char *
i4_gdkpixbuf_to_base64 (GdkPixbuf *pixbuf)
{
	int i, x, y, ch, rs;
	guchar *pix, *p;
	guchar *image;
	guchar lo, hi;
	char *base_string, *base64;

	if (OLED_WIDTH != gdk_pixbuf_get_width (pixbuf))
		return NULL;

	if (OLED_HEIGHT != gdk_pixbuf_get_height (pixbuf))
		return NULL;

	ch = gdk_pixbuf_get_n_channels (pixbuf);
	rs = gdk_pixbuf_get_rowstride (pixbuf);
	pix = gdk_pixbuf_get_pixels (pixbuf);

	image = g_malloc (MAX_IMAGE_SIZE);
	i = 0;
	for (y = 0; y < OLED_HEIGHT; y++) {
		for (x = 0; x < (OLED_WIDTH / 2); x++) {
			p = pix + y * rs + 2 * x * ch;
			hi = 0xf0 & ((p[0] + p[1] + p[2])/ 3 * p[3] / 0xff);
			p = pix + y * rs + ((2 * x) + 1) * ch;
			lo = 0x0f & (((p[0] + p[1] + p[2])/ 3 * p[3] / 0xff) >> 4);
			image[i] = hi | lo;
			i++;
		}
	}

	//FIXME Add i4_image_to_base64 function
	base_string = g_base64_encode (image, MAX_IMAGE_SIZE);
	base64 = g_strconcat (MAGIC_BASE64, base_string, NULL);
	g_free (base_string);
	g_free (image);
	return base64;
}

unsigned char*
i4_base64_to_image (gchar *label)
{
	char *base_string;
	unsigned char *image;
	gsize length;

	base_string = g_strdup (label + MAGIC_BASE64_LEN);
	image = g_base64_decode ((const char*)base_string, &length);

	g_free (base_string);
	return image;
}

GdkPixbuf *
i4_base64_to_gdkpixbuf (gchar *label)
{
	unsigned char *image;
	GdkPixbuf *pixbuf;
	int i, y, x, ch, rs;
	guchar *pix, *p;

	image = i4_base64_to_image (label);
	pixbuf = gdk_pixbuf_new (GDK_COLORSPACE_RGB, 0, 8, OLED_WIDTH, OLED_HEIGHT);
	ch = gdk_pixbuf_get_n_channels (pixbuf);
	rs = gdk_pixbuf_get_rowstride (pixbuf);
	pix = gdk_pixbuf_get_pixels (pixbuf);

	i = 0;
	for (y = 0; y < OLED_HEIGHT; y++) {
		for (x = 0; x < (OLED_WIDTH / 2); x++) {
			p = pix + y * rs + 2 * x * ch;
			p[0] = p[1] = p[2] = image[i] & 0xf0;
			p[3] = 0xff; /* Black background */
			p = pix + y * rs + ((2 * x) + 1) * ch;
			p[0] = p[1] = p[2] = (image[i] & 0x0f) << 4;
			p[3] = 0xff; /* Black background */
			i++;
		}
	}
	g_free (image);
	return pixbuf;
}

char *
i4_gdkpixbuf_to_base64_scrambled (GdkPixbuf *pixbuf)
{
	int i, x, y, ch, rs;
	guchar *pix, *p;
	guchar *image;
	guchar lo, hi;
	char *base_string, *base64;

	if (OLED_WIDTH != gdk_pixbuf_get_width (pixbuf))
		return NULL;

	if (OLED_HEIGHT != gdk_pixbuf_get_height (pixbuf))
		return NULL;

	ch = gdk_pixbuf_get_n_channels (pixbuf);
	rs = gdk_pixbuf_get_rowstride (pixbuf);
	pix = gdk_pixbuf_get_pixels (pixbuf);

	image = g_malloc (MAX_IMAGE_SIZE);
	i = 0;
	for (y = 0; y < OLED_HEIGHT; y++) {
		for (x = 0; x < (OLED_WIDTH / 2); x++) {
			p = pix + y * rs + 2 * x * ch;
			hi = 0xf0 & ((p[0] + p[1] + p[2])/ 3 * p[3] / 0xff);
			p = pix + y * rs + ((2 * x) + 1) * ch;
			lo = 0x0f & (((p[0] + p[1] + p[2])/ 3 * p[3] / 0xff) >> 4);
			image[i] = hi | lo;
			i++;
		}
	}

	base_string = g_base64_encode (image, MAX_IMAGE_SIZE);
	base64 = g_strconcat (MAGIC_BASE64, base_string, NULL);
	g_free (base_string);
	g_free (image);
	return base64;
}

static void
i4_split_text (const gchar *label, gchar *line1, gchar *line2)
{
	gchar delimiters[5] = "+-_ ";
	gchar **token;
	int token_len[MAX_TOKEN];
	gsize length;
	int i, label_len;

	label_len = strlen (label);
	if (g_utf8_strlen (label, LABEL_SIZE) <= MAX_1ST_LINE_LEN) {
		g_utf8_strncpy (line1, label, MAX_1ST_LINE_LEN);
		return;
	}
	token = g_strsplit_set (label, delimiters, -1);

	if (g_utf8_strlen (token[0], label_len) > MAX_1ST_LINE_LEN) {
		g_utf8_strncpy (line1, label, MAX_1ST_LINE_LEN);
		g_utf8_strncpy (line2, label + MAX_1ST_LINE_LEN, LABEL_SIZE - MAX_1ST_LINE_LEN);
		return;
	}

	for (i = 0; token[i] != NULL; i++) {
		token_len[i] = g_utf8_strlen (token[i], label_len);
	}

	length = token_len[0];
	i = 0;
	while ((length + token_len[i + 1] + 1) <= MAX_1ST_LINE_LEN) {
		i++;
		length = length + token_len[i] + 1;
	}

	g_utf8_strncpy (line1, label, length);
	g_utf8_strncpy (line2, g_utf8_offset_to_pointer (label, g_utf8_strlen (line1, label_len) + 1), label_len);

	return;
}

/* Free image when finished required */
guchar*
i4_csurf_to_image (cairo_surface_t *surface)
{
	guchar *image;
	unsigned char *csurf;
	int i, x, y;
	unsigned char lo, hi;
/* This will be used for bluetooth
*	unsigned char b0,b1,b2,b3,b4,b5,b6,b7;
*/

	image = g_malloc (MAX_IMAGE_SIZE);

	cairo_surface_flush(surface);
	csurf = cairo_image_surface_get_data(surface);
	i = 0;
/* This will be used for bluetooth
*	if (!params->bt_flag) {*/
		for (y = 0; y < OLED_HEIGHT; y++) {
			for (x = 0; x < (OLED_WIDTH >> 1); x++) {
				hi = 0xf0 & csurf[4 * OLED_WIDTH * y + 8 * x + 1];
				lo = 0x0f & (csurf[4 * OLED_WIDTH * y + 8 * x + 5] >> 4);
				image[i] = hi | lo;
				i++;
			}
		}
/* This will be used for bluetooth
*	} else {
		for (y = 0; y < OLED_HEIGHT; y++) {
			for (x = 0; x < (OLED_WIDTH >> 3); x++) {
				b0 = 0b10000000 & (csurf[4 * OLED_WIDTH * y + 32 * x +  1] >> 0);
				b1 = 0b01000000 & (csurf[4 * OLED_WIDTH * y + 32 * x +  5] >> 1);
				b2 = 0b00100000 & (csurf[4 * OLED_WIDTH * y + 32 * x +  9] >> 2);
				b3 = 0b00010000 & (csurf[4 * OLED_WIDTH * y + 32 * x + 13] >> 3);
				b4 = 0b00001000 & (csurf[4 * OLED_WIDTH * y + 32 * x + 17] >> 4);
				b5 = 0b00000100 & (csurf[4 * OLED_WIDTH * y + 32 * x + 21] >> 5);
				b6 = 0b00000010 & (csurf[4 * OLED_WIDTH * y + 32 * x + 25] >> 6);
				b7 = 0b00000001 & (csurf[4 * OLED_WIDTH * y + 32 * x + 29] >> 7);
				params->image[i] = b0 | b1 | b2 | b3 | b4 | b5 | b6 | b7;
				i++;
			}
		}
	}*/
	return (image);
}

//Call cairo_surface_destroy(surface) when finished with the surface required
cairo_surface_t*
i4_text_to_surface (const gchar *text)
{
	cairo_t *cr;
	cairo_surface_t *surface;
	PangoFontDescription *desc;
	PangoLayout *layout;
	int width, height;
	double dx, dy;
	char line1[LABEL_SIZE+1] = "";
	char line2[LABEL_SIZE+1] = "";
	char buf[LABEL_SIZE+1];

	i4_split_text(text, line1, line2);
	strcpy(buf, line1);
	strcat(buf, "\n");
	strcat(buf, line2);
	surface = cairo_image_surface_create(CAIRO_FORMAT_ARGB32, OLED_WIDTH, OLED_HEIGHT);
	cr = cairo_create(surface);
	cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 0.99);
	cairo_paint(cr);

	layout = pango_cairo_create_layout(cr);
	pango_layout_set_alignment(layout, PANGO_ALIGN_CENTER);
	pango_layout_set_text(layout, buf, -1);
	desc = pango_font_description_new();

	pango_font_description_set_family(desc, "DejaVu");
	pango_font_description_set_absolute_size(desc, PANGO_SCALE * 11);
	pango_layout_set_font_description(layout, desc);
	pango_font_description_free(desc);

	pango_layout_get_size(layout, &width, &height);
	width = width/PANGO_SCALE;
	cairo_new_path(cr);

	dx = trunc(((double)OLED_WIDTH - width)/2);

	if (!strcmp(line2, ""))
		dy = 10;
	else
		dy = 4;

	cairo_move_to(cr, dx, dy);
	cairo_set_source_rgba(cr, 1.0, 1.0, 1.0, 1.0);
	pango_cairo_update_layout(cr, layout);

	pango_cairo_layout_path(cr, layout);
	cairo_fill(cr);

	g_object_unref(layout);
	cairo_destroy(cr);

	return (surface);
}

guchar*
i4_text_to_image (const gchar *text)
{
	cairo_surface_t *surface;
	guchar *image;
	surface = i4_text_to_surface (text);
	image = i4_csurf_to_image(surface);
	cairo_surface_destroy(surface);
	return (image);
}
