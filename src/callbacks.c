#include "app.h"
#include "functions.h"

#define xstr(S) str(S)
#define str(S) #S

#define ENTRY_CB(BUTTON_ID)									\
	void button##BUTTON_ID##_entry_changed_cb(GtkEntry* entry, t_app *app)			\
	{											\
		guchar *image;									\
		gchar *button_name;								\
		char *base_string, *base64;							\
		button_name = g_strconcat ("button", str(BUTTON_ID), "/", NULL);		\
		i4_dconf_write(button_name, gtk_entry_get_text(entry), app);			\
		image = i4_text_to_image(gtk_entry_get_text(entry));				\
		base_string = g_base64_encode (image, MAX_IMAGE_SIZE);				\
		base64 = g_strconcat (MAGIC_BASE64, base_string, NULL);				\
		i4_set_pixbuf_from_label (button_name, base64, app);				\
		g_free (button_name);								\
		g_free (base_string);								\
		g_free (image);									\
	}

ENTRY_CB(B);
ENTRY_CB(C);
ENTRY_CB(D);
ENTRY_CB(E);
ENTRY_CB(F);
ENTRY_CB(G);
ENTRY_CB(H);
ENTRY_CB(I);

#define IMAGE_CB(BUTTON_ID)										\
void button##BUTTON_ID##_image_button_press_event_cb(GtkWidget* widget, GdkEvent *event, t_app *app)	\
{													\
	GtkDialog* select_icon_window;									\
	app->current_button = g_strconcat("button", str(BUTTON_ID), "/", NULL);				\
	create_window (app);										\
	select_icon_window = GTK_DIALOG (app_get_ui_element(app, "select_icon_window"));		\
	gtk_widget_realize(GTK_WIDGET(select_icon_window));						\
}

IMAGE_CB(B);
IMAGE_CB(C);
IMAGE_CB(D);
IMAGE_CB(E);
IMAGE_CB(F);
IMAGE_CB(G);
IMAGE_CB(H);
IMAGE_CB(I);

void
toggle_button_toggled_cb(GtkToggleButton *tb, t_app *app)
{
	GET_UI_ELEMENT(GtkIconView, icon_view);
	gtk_icon_view_set_model (icon_view, create_and_fill_model(app));
	
}

void
icon_view_item_activated_cb(GtkWidget* widget, GdkEvent *event, t_app *app)
{
	GET_UI_ELEMENT(GtkWindow, select_icon_window);
	gtk_dialog_response (GTK_DIALOG (select_icon_window), GTK_RESPONSE_OK);
}

void
icon_view_selection_changed_cb (GtkIconView *icon_view, t_app *app)
{
	GList *list;
	GET_UI_ELEMENT(GtkDialog, select_icon_window);

	list = gtk_icon_view_get_selected_items (icon_view);
	gtk_dialog_set_response_sensitive (GTK_DIALOG (select_icon_window),
			GTK_RESPONSE_APPLY,
			(list != NULL));

	g_list_free_full (list, (GDestroyNotify) gtk_tree_path_free);
}

void
on_select_icon_window_response (GtkDialog *select_icon_window, int response_id, t_app * app)
{
	GtkTreePath *path;
	GtkTreeIter iter;
	gchar *base64_label;
	GdkPixbuf *pixbuf;
	GtkTreeModel *model;
	GList *selected;

	if ((response_id == GTK_RESPONSE_APPLY) || (response_id == GTK_RESPONSE_OK)) {
		GET_UI_ELEMENT(GtkIconView, icon_view);

		selected = gtk_icon_view_get_selected_items (icon_view);
		if (!selected)
			goto out;

		model = gtk_icon_view_get_model (icon_view);
		path = (GtkTreePath *)selected->data;
		gtk_tree_model_get_iter (model, &iter, path);
		gtk_tree_path_free (path);
		gtk_tree_model_get (model, &iter, COL_PIXBUF, &pixbuf, -1);
		// If this patch is in gnome https://bug724955.bugzilla-attachments.gnome.org/attachment.cgi?id=271795
		// we can do some cleaning here 
		i4_set_text_for_button(app->current_button, "", app);
		i4_set_pixbuf_for_button (app->current_button, pixbuf, app);
		base64_label = i4_gdkpixbuf_to_base64_scrambled (pixbuf);
		//FIXME unref pixbuf here
		i4_dconf_write (app->current_button, base64_label, app);
		//
		//
		g_free(base64_label);
		g_list_free (selected);
	}
/*	if (response_id == GTK_RESPONSE_CANCEL) {
		fprintf (stderr,"response: %d GTK_RESPONSE_CANCEL", response_id);
	}
*/
out:
	gtk_widget_hide (GTK_WIDGET (select_icon_window));
}

