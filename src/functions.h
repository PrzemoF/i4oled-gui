#ifndef __FUNC__
#define __FUNC__

gchar* i4_strip_last(gchar*);
void i4_split_text (const gchar*, gchar*, gchar*);
void i4_scramble_image (guchar*);
void i4_descramble_image (guchar*);
cairo_surface_t* i4_text_to_surface (const gchar *);
char* i4_gdkpixbuf_to_base64 (GdkPixbuf*);
char* i4_gdkpixbuf_to_base64_scrambled (GdkPixbuf*);
GdkPixbuf* i4_base64_to_gdkpixbuf (gchar*);
unsigned char* i4_base64_to_image (gchar*);
guchar* i4_text_to_image (const gchar*);

#endif
