#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/dir.h>
#include <sys/param.h>

#include "app.h"
#include "functions.h"

void
i4_show_error_dialog(t_app *app)
{
	GtkWidget *dialog, *label;
	GtkDialogFlags flags = GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT;
	GET_UI_ELEMENT (GtkWindow, app_window);

	dialog = gtk_dialog_new_with_buttons ("i4oled-gui", app_window, flags, "OK", GTK_RESPONSE_ACCEPT, NULL);
	//This is rather ugly error message box...
	label = gtk_label_new ("	No wacom tablet detected! Please send results of \"dconf list "WACOM_DEV_DIR" to the developer.	    ");
	g_signal_connect_swapped (dialog, "response", G_CALLBACK (gtk_widget_destroy), dialog);
	gtk_container_add (GTK_CONTAINER (gtk_dialog_get_content_area (GTK_DIALOG (dialog))), label);
	gtk_widget_show_all (GTK_WIDGET (dialog));
	gtk_dialog_run (GTK_DIALOG (dialog));
	app->critical_error = 1;
}

extern  int alphasort();

int
file_select(const struct direct *entry)
{
	if ((strcmp(entry->d_name, ".") == 0) || (strcmp(entry->d_name, "..") == 0))
		return (FALSE);
	else
		return (TRUE);
}

void
i4_add_files_to_list(GtkListStore *list_store, char *path)
{
	GdkPixbuf *p;
	GtkTreeIter iter;
	GError *err = NULL;
	gchar *filename;
	int count,i;
	struct direct **files;

	count = scandir(path, &files, file_select, alphasort);
	if (count <= 0) {
		//fprintf(stderr, "No files in this directory %s\n", path); //FIXME this is normal, do not warn about it, debug message
		goto out;
	}

	for (i = 1; i < count + 1; ++i) {
		filename = g_strdup(files[i - 1]->d_name);
		p = gdk_pixbuf_new_from_file (g_strconcat(path, filename, NULL), &err);
		if (OLED_WIDTH != gdk_pixbuf_get_width (p) || (OLED_HEIGHT != gdk_pixbuf_get_height (p)))
			continue;

		gtk_list_store_append (list_store, &iter);
		gtk_list_store_set (list_store, &iter, COL_PIXBUF, p, -1);
	}
out:
	return;
}

GtkTreeModel *
create_and_fill_model (t_app* app)
{
	GtkListStore *list_store;
	gchar *user_path;

	user_path = g_strconcat (g_get_home_dir (), "/", WACOM_ICONS_USER_DIR, NULL);

	list_store = gtk_list_store_new (NUM_COLS, GDK_TYPE_PIXBUF);

	GET_UI_ELEMENT (GtkToggleButton, toggle_button_system );
	GET_UI_ELEMENT (GtkToggleButton, toggle_button_user );

	if (gtk_toggle_button_get_active (toggle_button_system)) {
		i4_add_files_to_list(list_store, WACOM_ICONS_SYS_DIR);
		i4_add_files_to_list(list_store, WACOM_ICONS_SYS_LOCAL_DIR);
	}
	if (gtk_toggle_button_get_active (toggle_button_user))
		i4_add_files_to_list(list_store, user_path);
	return GTK_TREE_MODEL (list_store);
}

void
create_window (t_app * app)
{
	GtkWidget *select_icon_window;
	GtkIconView *icon_view;

	select_icon_window = (GtkWidget*) app_get_ui_element(app, "select_icon_window");
	icon_view = (GtkIconView*) app_get_ui_element(app, "icon_view");
	gtk_icon_view_set_model (icon_view, create_and_fill_model(app));

	gtk_icon_view_set_pixbuf_column (GTK_ICON_VIEW (icon_view), COL_PIXBUF);
	gtk_widget_show (select_icon_window);
}

GVariant*
i4_get_key(gchar *button, gchar *key, t_app *app)
{
	return dconf_client_read (app->dconf_client, g_strconcat(app->dconf_dir, button, key, NULL));
}

void
i4_dconf_write(gchar *button_name, const gchar *label, t_app *app)
{
	gchar* dir;
	GError **error = NULL;
	GVariant *key_value = NULL;
	dir = g_strconcat(app->dconf_dir, button_name, WACOM_OLED_KEY, NULL);
	key_value = g_variant_new_string (label);
	dconf_client_write_fast (app->dconf_client, dir, key_value, error); //FIXME add error handling 
}

void
i4_set_text_for_button(gchar *button_name, gchar *label, t_app *app)
{
	char *button_id, *entry_name;
	GtkEntry* entry;

	button_id = i4_strip_last (button_name);
	entry_name =  g_strconcat(button_id, "_entry", NULL);
	entry = (GtkEntry *) app_get_ui_element(app, entry_name);
	gtk_entry_set_text (entry, label);
	g_free (button_id);
}

void
i4_set_pixbuf_for_button(gchar *button_name, GdkPixbuf *pixbuf, t_app *app)
{
	GtkImage *image_gtk;
	gchar *button_id, *image_gtk_name;

	button_id = i4_strip_last (button_name);
	image_gtk_name =  g_strconcat(button_id, "_image", NULL);
	image_gtk = (GtkImage *) app_get_ui_element(app, image_gtk_name);
	gtk_image_set_from_pixbuf (image_gtk, pixbuf);
	g_free (button_id);
}

void
i4_set_pixbuf_from_label (gchar *button_name, gchar *label, t_app *app)
{
	GdkPixbuf *pixbuf;
	pixbuf = i4_base64_to_gdkpixbuf (label);
	i4_set_pixbuf_for_button(button_name, pixbuf, app);
	g_object_unref(pixbuf);
}

void
i4_set_entry(gchar *button_name, gchar *label, t_app *app)
{
	if (button_name) {
		if (g_str_has_prefix(label, MAGIC_BASE64)) {
			i4_set_pixbuf_from_label (button_name, label, app);
			i4_set_text_for_button (button_name, "", app);
		} else {
			//FIXME - render text to pixbuf here
			i4_set_text_for_button (button_name, label, app);
		}
	}
}

void
gui_init_from_dconf (t_app *app)
{
	GVariant *key_value = NULL;
	gchar *wacom_dir = WACOM_DEV_DIR;
	gchar *dev_dir = NULL;
	gchar *button_name;
	gchar *oled_label;
	gint len, i;
	gsize length;
	gchar **list;

	app->dconf_client = dconf_client_new();
	if (!app->dconf_client) {
		fprintf(stderr, "Call dconf_client_new() returnet NULL!\n");
		goto out;
	}

	list = dconf_client_list (app->dconf_client, wacom_dir, &len);
	if (!len) {
		fprintf(stderr, "Zero array returned by dconf_client_list! %s Cannot continue :-(\n", wacom_dir);
		i4_show_error_dialog (app);
		goto out;
	}

	/* FIXME Currently just work on first usb device */
	for (i = 0; list[i] != NULL; i++) {
		if (g_strrstr (list[i], "usb")) {
			dev_dir = g_strdup (list[i]);
			break;
		}
	}

	if (dev_dir) {
		app->dconf_dir = g_strconcat(wacom_dir, dev_dir, NULL);

		list = dconf_client_list (app->dconf_client, app->dconf_dir, &len);
		for (i = 0; list[i] != NULL; i++) {
			if (g_strrstr (list[i], "button")) {
				button_name = g_strdup (list[i]);
				key_value = i4_get_key(button_name, WACOM_OLED_KEY, app);
				if (key_value) {
					oled_label = g_strdup(g_variant_get_string(key_value, &length));
					i4_set_entry(button_name, oled_label, app);
					g_free(oled_label);
				}
				g_free(button_name);
			}
		}
	}
	g_free(dev_dir);

out:
	return;
}

inline GObject *
app_get_ui_element (t_app *app, const gchar *name)
{
	return gtk_builder_get_object(app->definitions, name);
} 

void
app_init (t_app *app)
{
	GError *err = NULL;

	app->definitions = gtk_builder_new ();
	gtk_builder_add_from_file (app->definitions, UI_DEFINITIONS_FILE, &err);

	if (err != NULL) {
		g_printerr ("Error while loading app definitions file: %s\n", err->message);
		g_error_free (err);
		gtk_main_quit ();
	}

	gtk_builder_connect_signals (app->definitions, app);
	app->objects = gtk_builder_get_objects (app->definitions);
}
